﻿
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>
#include <string>
using namespace std;
int main()
{
    setlocale(LC_ALL, "rus");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    ifstream in("input.txt");
    ofstream out("output.txt");
    int N;
    in >> N;
    string word;
    int wordLength;
    int count[256];
    int wrong = 0;
    string trueWords[100];
    int ind = 0;
    while (!in.eof())
    {
        wrong = 0;
        in >> word;
        wordLength = word.length();
        for (int i = 0; i < 256; i++)
        {
            count[i] = 0;
        }
        for (int i = 0; i < wordLength; i++)
        {
            if ((char(word[i]) >= 97 && char(word[i] <= 122)) || (char(word[i]) >= 224 && char(word[i]) <= 255))
                count[word[i] - 32] += 1;
            else
                count[word[i]] += 1;
            if (count[word[i]] > 1)
            {
                wrong = 1;
                break;
            }
        }
        if (wrong == 0)
        {
            trueWords[ind] = word;
            ind += 1;
        }

    }
    for (int i = 1; i < 100; i++)
    {
        if ((trueWords[i - 1].length() < trueWords[i].length()))
        {
            swap(trueWords[i - 1], trueWords[i]);
        }
    }
    
    for (int i = 0; i < 100; i++)
    {
        if (trueWords[i] != "")
        {
            cout << trueWords[i] << endl;
        }
    }
}
